# Objective
Response the tech review of ExpressVPN

# Question
## How do you test your code?
The first thing is that, you should have a mindset that you should code a testable code which is mean you should decoupling your function and think more than just let it works.

For instance
```
func Foo(input string) string {
    strTmp := input + "I go to school by bus"
    strTmp = fmt.Spirntf("%s%v", strTmp, time.Now().Unix())
    return strTmp
}
```

We should refactor function as following
```
func Foo (input string) string {
    strTmp := NewString(input)
    strTmp = GetStringWithTimeNow(strTmp)
    return strTmp
}
```

Now u can check NewString, GetStringWithTimeNow outside the Foo function. Two of them is independent without hidden effect.
am not fully trust TDD as u always cannot know all the requirements at the first place. Just try your best to ensure all your small components is tested before u submit a PR. Plus, if u need to deal with others developer with performance please also submit your benchmark.
![](http://www.testingreferences.com/pyramids/Bagmar2012.jpg)

I also believe that profiling is part of testing. Sometime, Are bad code is work to cost a lot of money ( CPU time ). In order to spot an issue, we should have a stress test for hottest place in the application.
## What are some examples of apps that you’ve built? What was your role in the project and which pieces were your responsibility?

I am working in public API in lalamove. It enables our enterprise customer placing order via API rather than mobile application.

I do not get involved in the API design while the project start (as it outsources to a developer at the beginning). On the end of previous year around July I become the guy to develop this project.

After I get full control of this project, I do setup a GitlabCI in my small rasp pi and write around 50% unit test to ensure each commit will have a green light.

However, my friend teach me that an API should use json schema to ensure the incoming request is 4xx bad request or not. Rather than check the input in difference places. So, I create a small prototype about Golang jsonschema based on https://github.com/xeipuuv/gojsonschema (it has a long story). As I am so junior and do not pull enough simple data. I do not get success deployed at the first ( roll back two time ). Later on, I pull around 30 day incoming requests and add all of it to my unit test to ensure nothing will be broken. The reason is that many of our customer do not know the difference between empty string, empty object and null and 'undefined'. So the existing one allows all of it to send and parse which teach me a lot.

Besides, My boss ask me to fix a CPU issue in AWS, I run a set of benchmark with [https://github.com/google/pprof](https://github.com/google/pprof) then we find an issue about one of the library we used. It use a lot of CPU time so we report it to Google [https://github.com/googlemaps/google-maps-services-go/issues/125](https://github.com/googlemaps/google-maps-services-go/issues/125). In order to resolve it, I write a small library for very simple HTTP request [https://github.com/AlphaWong/google-geolocate](https://github.com/AlphaWong/google-geolocate).

Currently, this project is moving to k8s, so I create a company logger formatter based on uber-go/zap as following [https://github.com/lalamove-go/logs](https://github.com/lalamove-go/logs) to ensure all the go project share the same logging standard in Kibana.

So, I think the role of my current job is a humility learner and a guy response to product manager.

Sorry for my english. It is quite bad.

## In a few sentences, please describe how you judge the quality of code.
When I am the project master. I will review each PR as the following. Developer should not copy and paste everywhere. It is hard to maintance and it will produce inconsist behaviour. I will also setup a linter to ensure all developer will not fight for `tab` and `space`.
Also, developer must provider a testcase to protect their logic which enable CI to test it for each commit and avoid hidden change. I alwasys believe code is for computer, comment is for human. Developer should always think about reliability.
Developer should not step out while someone told u about some subjective view, u should justify for what u believe, If it is doubt, benchmark it which is most objective way.

Reference https://go-proverbs.github.io

## Using your favourite language and framework, assume you're developing an application that consumes a third-party HTTP API. Please write a very simple unit test-case that proves that your application handles http404 errors as expected. It doesn't matter what exactly that expected behaviour is, just show us how you structure a test and write expectations/assertions.
Here is my project (https://gitlab.com/AlphaWong/SimpleHTTP404) please review my code. Thx
